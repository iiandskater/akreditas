/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.30-MariaDB : Database - akreditas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `db_retnoer`;

/*Table structure for table `borang` */

DROP TABLE IF EXISTS `borang`;

CREATE TABLE `borang` (
  `borang_id` int(11) NOT NULL AUTO_INCREMENT,
  `borang_file` varchar(255) NOT NULL,
  `borang_note` text,
  `borang_by` int(11) NOT NULL,
  `borang_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`borang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `borang` */

insert  into `borang`(`borang_id`,`borang_file`,`borang_note`,`borang_by`,`borang_uploaded`) values 
(7,'REGPMB_FORM_ONLINE_17022532.pdf','',2,'2018-04-18 11:57:26'),
(8,'KARTU_TES_170225321.pdf','',2,'2018-04-18 13:07:17');

/*Table structure for table `borang_dokumen` */

DROP TABLE IF EXISTS `borang_dokumen`;

CREATE TABLE `borang_dokumen` (
  `id_dokumen` int(11) NOT NULL AUTO_INCREMENT,
  `id_keb_dok` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL COMMENT 'Uploader',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_dokumen`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `borang_dokumen` */

insert  into `borang_dokumen`(`id_dokumen`,`id_keb_dok`,`nama`,`file`,`id_user`,`status`) values 
(1,1,'Contoh Dokumen','Contoh Dokumen.docx',3,'0');

/*Table structure for table `borang_elemen` */

DROP TABLE IF EXISTS `borang_elemen`;

CREATE TABLE `borang_elemen` (
  `id_elemen` int(11) NOT NULL AUTO_INCREMENT,
  `id_master` int(11) NOT NULL,
  `butir` int(11) DEFAULT NULL,
  `elemen` text,
  `bobot` decimal(4,2) DEFAULT NULL,
  `skor` decimal(4,2) DEFAULT NULL,
  `justifikasi` enum('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik') DEFAULT NULL,
  `penilaian` text,
  PRIMARY KEY (`id_elemen`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `borang_elemen` */

insert  into `borang_elemen`(`id_elemen`,`id_master`,`butir`,`elemen`,`bobot`,`skor`,`justifikasi`,`penilaian`) values 
(1,1,1,'Test judul elemen',1.00,20.00,'Baik','Memiliki tata pamong yang (1) Kredibel (2) Transparan (3) ...'),
(2,2,1,'Test judul elemen',NULL,NULL,NULL,NULL),
(3,1,2,'Elemen no 2',0.01,0.01,'Baik','12345\r\n'),
(4,1,3,'kjbk bkkk f',1.00,11.00,'Sangat Baik','qwety');

/*Table structure for table `borang_keb_dok` */

DROP TABLE IF EXISTS `borang_keb_dok`;

CREATE TABLE `borang_keb_dok` (
  `id_keb_dok` int(11) NOT NULL AUTO_INCREMENT,
  `id_elemen` int(11) NOT NULL,
  `kebutuhan` varchar(255) NOT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_keb_dok`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `borang_keb_dok` */

insert  into `borang_keb_dok`(`id_keb_dok`,`id_elemen`,`kebutuhan`,`pic`,`deskripsi`) values 
(1,1,'Dokumen etika dosen','Waket 1','Testing dok'),
(2,1,'Dokumen etika mahasiswa','Waket 3',NULL),
(3,1,'Dokumen ABC part 2','Saya','122346');

/*Table structure for table `borang_master` */

DROP TABLE IF EXISTS `borang_master`;

CREATE TABLE `borang_master` (
  `id_master` int(10) NOT NULL AUTO_INCREMENT,
  `prodi` enum('ti','mi') NOT NULL DEFAULT 'ti',
  `id_tahun` int(11) NOT NULL,
  `tipe_buku` enum('III A','III B') NOT NULL DEFAULT 'III A',
  `standar` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  PRIMARY KEY (`id_master`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `borang_master` */

insert  into `borang_master`(`id_master`,`prodi`,`id_tahun`,`tipe_buku`,`standar`,`judul`) values 
(1,'ti',1,'III A',1,'Visi, Misi, Tujuan Dan Sasaran, Serta Strategi Pencapaian'),
(3,'ti',1,'III A',3,'Test input judul 2 edit');

/*Table structure for table `borang_tahun` */

DROP TABLE IF EXISTS `borang_tahun`;

CREATE TABLE `borang_tahun` (
  `id_tahun` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(4) NOT NULL,
  `oleh` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tahun`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `borang_tahun` */

insert  into `borang_tahun`(`id_tahun`,`tahun`,`oleh`,`status`) values 
(1,'2011',1,'1'),
(5,'2017',3,'1');

/*Table structure for table `ref_jabatan` */

DROP TABLE IF EXISTS `ref_jabatan`;

CREATE TABLE `ref_jabatan` (
  `jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan_name` varchar(50) NOT NULL,
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ref_jabatan` */

insert  into `ref_jabatan`(`jabatan_id`,`jabatan_name`) values 
(1,'Test'),
(3,'Staff'),
(4,'Mahasiswa');

/*Table structure for table `ref_user_grup` */

DROP TABLE IF EXISTS `ref_user_grup`;

CREATE TABLE `ref_user_grup` (
  `user_grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_grup_name` varchar(100) NOT NULL,
  PRIMARY KEY (`user_grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ref_user_grup` */

insert  into `ref_user_grup`(`user_grup_id`,`user_grup_name`) values 
(2,'S1 Teknik Informatika'),
(3,'BPMP'),
(4,'Ketua');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_role` enum('0','1','2','3','4') NOT NULL DEFAULT '4' COMMENT '0=superuser, 1=spm, 2=kaprodi, 3=panitia, 4=other',
  `user_by` int(11) NOT NULL DEFAULT '1',
  `user_npk` varchar(50) DEFAULT NULL,
  `user_grup_id` int(11) DEFAULT NULL,
  `user_grup` varchar(100) DEFAULT NULL,
  `user_jabatan_id` int(11) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `user_telp` varchar(20) DEFAULT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQUE` (`user_username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_username`,`user_password`,`user_name`,`user_role`,`user_by`,`user_npk`,`user_grup_id`,`user_grup`,`user_jabatan_id`,`user_jabatan`,`user_telp`,`user_created`,`user_active`) values 
(1,'admin','21232f297a57a5a743894a0e4a801fc3','Administrator','0',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-19 09:37:08','1'),
(2,'kaprodi','3c13922905d2bc454cc35e665335e2fd','Kaprodi 1','2',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-29 15:36:23','1'),
(3,'spm','51762626b4f785729159fd35eea74deb','SPM 1','1',1,'099887766554322',3,NULL,3,NULL,'089786754312','2018-05-29 15:36:26','1'),
(4,'panitia','d32b1673837a6a45f795c13ea67ec79e','Panitia 1','3',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-03 21:38:58','1'),
(5,'abc','abc','abc defg','2',3,'1234551',2,NULL,1,NULL,'12324','2018-06-11 12:41:57','1'),
(6,'asd','7815696ecbf1c96e6894b779456d330e','asdaaaaa','2',3,'2312312312',2,NULL,1,NULL,'12121212','2018-06-11 12:26:40','1'),
(7,'pana','pan','PAN','3',6,'123142131',2,NULL,3,NULL,'232342','2018-06-11 12:38:23','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
