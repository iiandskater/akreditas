-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 08, 2018 at 03:25 PM
-- Server version: 5.5.60
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_retnoer`
--

-- --------------------------------------------------------

--
-- Table structure for table `borang_dokumen`
--

CREATE TABLE `borang_dokumen` (
  `id_dokumen` int(11) NOT NULL,
  `id_keb_dok` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL COMMENT 'Uploader',
  `status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_dokumen`
--

INSERT INTO `borang_dokumen` (`id_dokumen`, `id_keb_dok`, `nama`, `file`, `id_user`, `status`) VALUES
(1, 1, 'Contoh Dokumen', 'Contoh Dokumen.docx', 3, '0'),
(2, 1, 'INSTRUMEN-MONEV-revisi 2', 'INSTRUMEN-MONEV-revisi 2.pdf', 3, '0'),
(3, 4, 'contoh1', 'contoh1.docx', 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `borang_elemen`
--

CREATE TABLE `borang_elemen` (
  `id_elemen` int(11) NOT NULL,
  `id_master` int(11) NOT NULL,
  `butir` varchar(10) DEFAULT NULL,
  `elemen` text,
  `bobot` decimal(4,2) DEFAULT NULL,
  `skor` decimal(4,2) DEFAULT NULL,
  `justifikasi` enum('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik') DEFAULT NULL,
  `penilaian` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_elemen`
--

INSERT INTO `borang_elemen` (`id_elemen`, `id_master`, `butir`, `elemen`, `bobot`, `skor`, `justifikasi`, `penilaian`) VALUES
(1, 1, '1', 'Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran program studi.', '1.04', '2.50', 'Cukup', 'Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik.'),
(2, 1, '2', 'Strategi pencapaian sasaran dengan rentang waktu yang jelas dan didukung oleh dokumen.', '1.04', '2.80', 'Cukup', 'Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan cukup realistik (2) didukung dokumen yang cukup lengkap.'),
(3, 1, '3', 'Sosialisasi visi-misi.  Sosialisasi yang efektif tercermin dari tingkat pemahaman seluruh pemangku kepentingan internal yaitu sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.', '1.04', '2.50', 'Cukup', 'Kurang dipahami oleh  sivitas akademika  dan tenaga kependidikan.\r\n'),
(4, 2, '1', 'Tata pamong menjamin terwujudnya visi, terlaksananya misi, tercapainya tujuan, berhasilnya strategi yang digunakan secara kredibel, transparan, akuntabel, bertanggung jawab, dan adil.', '1.39', '2.80', 'Cukup', 'Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut :  (1) Kredible, (2) Transparan, (3) akuntable (4) bertanggung jawab (5) adil'),
(10, 2, '2', 'Karakteristik kepemimpinan yang efektif (kepemimpinan operasional, kepemimpinan organisasi, kepemimpinan publik).', '0.69', '2.50', 'Cukup', 'Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut:  (1) kepemimpinan operasional, (2) kepemimpinan organisasi (3) Kepemimpinan publik.'),
(11, 2, '3', 'Sistem pengelolaan fungsional dan operasional program studi mencakup: planning, organizing, staffing, leading, controlling yang efektif dilaksanakan.', '1.39', '2.80', 'Cukup', 'Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap.\r\n'),
(12, 2, '4', 'Pelaksanaan penjaminan mutu di program studi.', '1.39', '2.50', 'Cukup', 'Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap.\r\n'),
(13, 2, '5', 'Penjaringan umpan balik  dan tindak lanjutnya.', '0.69', '2.50', 'Cukup', 'Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental.																							\r\n'),
(14, 2, '6', 'Upaya untuk menjamin keberlanjutan (sustainability) program studi.', '0.69', '2.80', 'Cukup', 'Ada bukti sebagian usaha ( > 3) dilakukan .									'),
(15, 8, '1', 'Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.', '0.72', '2.80', 'Cukup', 'Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(16, 4, '1', 'Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.', '0.72', '2.80', 'Cukup', 'Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(17, 5, '6', 'Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir.', '0.57', '2.80', 'Cukup', 'Upaya perbaikan dilakukan untuk 2 dari 4 yang seharusnya diperbaiki/ ditingkatkan.'),
(18, 6, '1', 'Keterlibatan program studi dalam perencanaan target kinerja, perencanaan kegiatan/ kerja dan perencanaan/alokasi dan pengelolaan dana.', '0.67', '2.50', 'Cukup', 'Program studi dilibatkan dalam perencanaan alokasi, namun pengelolaan dana dilakukan oleh Fakultas/Sekolah Tinggi.');

-- --------------------------------------------------------

--
-- Table structure for table `borang_keb_dok`
--

CREATE TABLE `borang_keb_dok` (
  `id_keb_dok` int(11) NOT NULL,
  `id_elemen` int(11) NOT NULL,
  `kebutuhan` varchar(255) NOT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_keb_dok`
--

INSERT INTO `borang_keb_dok` (`id_keb_dok`, `id_elemen`, `kebutuhan`, `pic`, `deskripsi`) VALUES
(1, 4, 'Dokumen etika dosen', 'Waket 1', ''),
(2, 4, 'Dokumen etika mahasiswa', 'Waket 3', ''),
(3, 4, 'Dokumen etika tenaga kependidikan', 'Waket 2', ''),
(4, 4, 'Dokumen tentang reward dan punishment', 'Waket 2', ''),
(5, 4, 'SOP layanan administrasi', 'Waket 1', ''),
(6, 4, 'SOP layanan perpustakaan', 'Waket 1', ''),
(7, 4, 'SOP layanan lab dan studi', 'Waket 1', ''),
(8, 4, 'SOP layanan lain jika ada', 'Waket 1', '');

-- --------------------------------------------------------

--
-- Table structure for table `borang_master`
--

CREATE TABLE `borang_master` (
  `id_master` int(10) NOT NULL,
  `prodi` enum('ti','mi') NOT NULL DEFAULT 'ti',
  `id_tahun` int(11) NOT NULL,
  `tipe_buku` enum('III A','III B') NOT NULL DEFAULT 'III A',
  `standar` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_master`
--

INSERT INTO `borang_master` (`id_master`, `prodi`, `id_tahun`, `tipe_buku`, `standar`, `judul`) VALUES
(1, 'ti', 2, 'III A', 1, 'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(2, 'ti', 2, 'III A', 2, 'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(3, 'ti', 2, 'III A', 3, 'MAHASISWA DAN LULUSAN'),
(4, 'ti', 2, 'III A', 4, 'SUMBER DAYA MANUSIA '),
(5, 'ti', 2, 'III A', 5, 'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(6, 'ti', 2, 'III A', 6, 'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(7, 'ti', 2, 'III A', 7, 'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA'),
(12, 'ti', 2, 'III B', 1, 'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(13, 'ti', 2, 'III B', 2, 'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(14, 'ti', 2, 'III B', 3, 'MAHASISWA DAN LULUSAN'),
(15, 'ti', 2, 'III B', 4, 'SUMBER DAYA MANUSIA '),
(16, 'ti', 2, 'III B', 5, 'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(17, 'ti', 2, 'III B', 6, 'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(18, 'ti', 2, 'III B', 7, 'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA');

-- --------------------------------------------------------

--
-- Table structure for table `borang_tahun`
--

CREATE TABLE `borang_tahun` (
  `id_tahun` int(11) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `oleh` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_tahun`
--

INSERT INTO `borang_tahun` (`id_tahun`, `tahun`, `oleh`, `status`) VALUES
(1, '2014', 1, '1'),
(2, '2017', 3, '1'),
(3, '2018', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jabatan`
--

CREATE TABLE `ref_jabatan` (
  `jabatan_id` int(11) NOT NULL,
  `jabatan_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jabatan`
--

INSERT INTO `ref_jabatan` (`jabatan_id`, `jabatan_name`) VALUES
(1, 'SPM'),
(2, 'Kaprodi'),
(3, 'Waket 1'),
(4, 'Waket 2'),
(5, 'Waket 3'),
(6, 'LPPM'),
(7, 'Perpus');

-- --------------------------------------------------------

--
-- Table structure for table `ref_user_grup`
--

CREATE TABLE `ref_user_grup` (
  `user_grup_id` int(11) NOT NULL,
  `user_grup_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_user_grup`
--

INSERT INTO `ref_user_grup` (`user_grup_id`, `user_grup_name`) VALUES
(1, 'SPM'),
(2, 'Kaprodi'),
(3, 'Panitia');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_role` enum('0','1','2','3','4') NOT NULL DEFAULT '4' COMMENT '0=superuser, 1=spm, 2=kaprodi, 3=panitia, 4=other',
  `user_by` int(11) NOT NULL DEFAULT '1',
  `user_npk` varchar(50) DEFAULT NULL,
  `user_grup_id` int(11) DEFAULT NULL,
  `user_grup` varchar(100) DEFAULT NULL,
  `user_jabatan_id` int(11) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `user_telp` varchar(20) DEFAULT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_password`, `user_name`, `user_role`, `user_by`, `user_npk`, `user_grup_id`, `user_grup`, `user_jabatan_id`, `user_jabatan`, `user_telp`, `user_created`, `user_active`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-19 02:37:08', '1'),
(2, 'kaprodi', '3c13922905d2bc454cc35e665335e2fd', 'Kaprodi 1', '2', 1, '2016.13.0087', 2, 'kaprodi', 2, 'kaprodi', '', '2018-07-08 11:50:39', '1'),
(3, 'spm', '51762626b4f785729159fd35eea74deb', 'SPM 1', '1', 1, '2016.13.0086', 3, NULL, 3, NULL, '089786754312', '2018-07-08 04:02:09', '1'),
(8, 'panitia', 'panitia', 'Waket 1', '3', 2, '0880', 3, NULL, 3, NULL, '', '2018-07-08 05:31:21', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `borang_dokumen`
--
ALTER TABLE `borang_dokumen`
  ADD PRIMARY KEY (`id_dokumen`);

--
-- Indexes for table `borang_elemen`
--
ALTER TABLE `borang_elemen`
  ADD PRIMARY KEY (`id_elemen`);

--
-- Indexes for table `borang_keb_dok`
--
ALTER TABLE `borang_keb_dok`
  ADD PRIMARY KEY (`id_keb_dok`);

--
-- Indexes for table `borang_master`
--
ALTER TABLE `borang_master`
  ADD PRIMARY KEY (`id_master`);

--
-- Indexes for table `borang_tahun`
--
ALTER TABLE `borang_tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `ref_user_grup`
--
ALTER TABLE `ref_user_grup`
  ADD PRIMARY KEY (`user_grup_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`user_username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `borang_dokumen`
--
ALTER TABLE `borang_dokumen`
  MODIFY `id_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `borang_elemen`
--
ALTER TABLE `borang_elemen`
  MODIFY `id_elemen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `borang_keb_dok`
--
ALTER TABLE `borang_keb_dok`
  MODIFY `id_keb_dok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `borang_master`
--
ALTER TABLE `borang_master`
  MODIFY `id_master` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `borang_tahun`
--
ALTER TABLE `borang_tahun`
  MODIFY `id_tahun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ref_user_grup`
--
ALTER TABLE `ref_user_grup`
  MODIFY `user_grup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
