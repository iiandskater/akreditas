<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_borang extends CI_Model {

	function get_master() {
		$this->db->select(
			'bm.*,
			(SELECT COUNT(*) FROM borang_elemen be WHERE be.id_master = bm.id_master) AS jml_elemen'
		);
		$this->db->where('prodi', $this->session->userdata('l_prodi'));
		$this->db->where('id_tahun', $this->session->userdata('l_tahun'));
		$this->db->where('tipe_buku', $this->session->userdata('l_buku'));
		$query = $this->db->get('borang_master bm')->result();
		
		return $query;
	}

	function get_master_info($id) {
		$this->db->where('prodi', $this->session->userdata('l_prodi'));
		$this->db->where('id_tahun', $this->session->userdata('l_tahun'));
		$this->db->where('tipe_buku', $this->session->userdata('l_buku'));
		$this->db->where('id_master', $id);
		$query = $this->db->get('borang_master')->result();
		
		return $query[0];
	}

	function insert_master($data) {
		$this->db->insert('borang_master', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function update_master($id, $data) {
		$this->db->where('id_master', $id);
		$this->db->update('borang_master', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function delete_master($id) {
		$this->db->where('id_master', $id);
		$ele = $this->db->get('borang_elemen')->result();
		foreach ($ele as $k => $v) {
			$this->delete_elemen($v->id_elemen);
		}

		$this->db->where('id_master', $id);
		$this->db->delete('borang_master');
	}

	function get_elemen($id_master) {
		$this->db->where('id_master', $id_master);
		$query = $this->db->get('borang_elemen')->result();
		
		return $query;
	}

	function get_elemen_info($id) {
		$this->db->where('id_elemen', $id);
		$query = $this->db->get('borang_elemen')->result();
		
		return $query[0];
	}

	function insert_elemen($data) {
		$this->db->insert('borang_elemen', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function update_elemen($id, $data) {
		$this->db->where('id_elemen', $id);
		$this->db->update('borang_elemen', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function delete_elemen($id) {
		$this->db->where('id_elemen', $id);
		$keb = $this->db->get('borang_keb_dok')->result();
		foreach ($keb as $k => $v) {
			$this->delete_keb_dok($v->id_keb_dok);
		}

		$this->db->where('id_elemen', $id);
		$this->db->delete('borang_elemen');
	}

	function get_keb_dok($id_elemen) {
		$this->db->where('id_elemen', $id_elemen);
		$query = $this->db->get('borang_keb_dok')->result();
		
		return $query;
	}

	function get_keb_dok_info($id) {
		$this->db->where('id_keb_dok', $id);
		$query = $this->db->get('borang_keb_dok')->result();
		
		return $query[0];
	}

	function insert_keb_dok($data) {
		$this->db->insert('borang_keb_dok', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function update_keb_dok($id, $data) {
		$this->db->where('id_keb_dok', $id);
		$this->db->update('borang_keb_dok', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return 3;
		}
	}

	function delete_keb_dok($id) {
		$this->db->where('id_keb_dok', $id);
		$dok = $this->db->get('borang_dokumen')->result();
		foreach ($dok as $k => $v) {
			$this->delete_dokumen($v->id_dokumen);
		}

		$this->db->where('id_keb_dok', $id);
		$this->db->delete('borang_keb_dok');
	}

	function get_dokumen($id_keb_dok) {
		$this->db->select(
			'bd.*,
			us.user_name AS oleh'
		);
		$this->db->where('id_keb_dok', $id_keb_dok);
		$this->db->join('user us', 'us.user_id=bd.id_user', 'left');
		$query = $this->db->get('borang_dokumen bd')->result();
		
		return $query;
	}

	function get_dokumen_info($id) {
		$this->db->where('id_dokumen', $id);
		$query = $this->db->get('borang_dokumen')->result();
		
		return $query[0];
	}

	function insert_dokumen($data) {
		$this->db->insert('borang_dokumen', $data);
		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function update_dokumen($id, $data) {
		$this->db->where('id_dokumen', $id);
		$this->db->update('borang_dokumen', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function delete_dokumen($id) {
		$file = $this->get_dokumen_info($id)->file;
		$dir = getcwd()."/docs/";
		@unlink($dir.$file);

		$this->db->where('id_dokumen', $id);
		$this->db->delete('borang_dokumen');
	}
}