<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_main extends CI_Model {

    function get_home() {
        $this->db->select(
            'bm.standar,
            bm.judul,
            (SELECT SUM(skor) FROM borang_elemen WHERE id_master=bm.id_master) AS nilai'
        );
        $this->db->where('prodi', $this->session->userdata('l_prodi'));
        $this->db->where('id_tahun', $this->session->userdata('l_tahun'));
        $this->db->where('tipe_buku', $this->session->userdata('l_buku'));
        $query = $this->db->get('borang_master bm')->result();
        
        return $query;
    }


    function get_tahun() {
        $this->db->where('status', '1');
        $query = $this->db->get('borang_tahun')->result();
        return $query;
    }

    function do_login($data) {
        $this->db->where('user_username', $data['l_user']);
        $this->db->where('user_password', md5($data['l_pass']));
        $query = $this->db->get('user')->result();

        if(sizeof($query)==1) {
            $return['uid'] = $query[0]->user_id;
            $return['name'] = $query[0]->user_name;
            $return['uname'] = $query[0]->user_username;
            $return['role'] = $query[0]->user_role;

            $return['l_prodi'] = $data['l_prodi'];

            if($data['l_prodi'] == 'in') {
                $return['i_prodi'] = 'Informatika';
            } elseif ($data['l_prodi'] == 'mi') {
                $return['i_prodi'] = 'Manajemen Informatika';
            } elseif ($data['l_prodi'] == 'ti') {
                $return['i_prodi'] = 'Teknologi Informasi';
            } elseif ($data['l_prodi'] == 'si') {
                $return['i_prodi'] = 'Sistem Informasi';
            }

            $this->db->where('id_tahun', $data['l_tahun']);
            $qtahun = $this->db->get('borang_tahun')->result();
            $return['l_tahun'] = $data['l_tahun'];
            $return['i_tahun'] = $qtahun['0']->tahun;

            $return['l_buku'] = $data['l_buku'];
            $return['i_buku'] = $data['l_buku'];
        } else {
            $return = false;
        }

        return $return;
    }

}