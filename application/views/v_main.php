<?php if(null==$this->session->userdata('uid') && null==$this->session->userdata('uname')) { redirect(base_url(), 'refresh'); } ?>

<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("comp/load_head"); ?>
</head>
<body class="hold-transition skin-blue-light sidebar-mini fixed">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?=base_url()?>" class="logo">
				<span class="logo-mini"><b>AKR</b></span>
				<span class="logo-lg"><b>Akreditasi</b></span>
			</a>
			<nav class="navbar navbar-static-top" role="navigation">
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?=base_url()?>assets/img/avatar2.png" class="user-image" alt="User Image">
								<span class="hidden-xs"><?=$this->session->userdata('uname')?></span>
							</a>

							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="<?=base_url()?>assets/img/avatar2.png" class="img-circle" alt="User Image">
									<p><?=$this->session->userdata('name')?></p>
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?=base_url()?>main/profile" class="btn btn-default">Profile</a>
									</div>
									<div class="pull-right">
										<a id="btn-logout" class="btn bg-maroon">Log out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar">
				<div class="user-panel">
				</div>
				<div class="user-panel hidden">
					<div class="pull-left image">
						<img src="assets/img/avatar2.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Admin</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>

				<form action="#" method="get" class="sidebar-form hidden">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</form>

				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">NAVIGASI</li>
					<li>
						<a href="<?=base_url()?>" class="navitem">
							<i class="fa fa-home"></i> <span>Home</span>
						</a>
					</li>					
					<?php 
					$role = $this->session->userdata('role'); 
					if($role==0) {
						$nav='admin';
					} elseif ($role==1) {
						$nav='kaprodi';
					} elseif ($role==2) {
						$nav='spm';
					} elseif ($role==3) {
						$nav='other';
					}
					$this->load->view('comp/nav_'.$nav);
					?>
				</ul>


			</section>
		</aside>

		<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>

		<div class="content-wrapper">
			<section class="content-header">
				<div align="center">
					<h3><?=$this->session->userdata('btitle')?></h3>
				</div>
				<hr />
				<h4 id="page_title"><?=$htitle?></h4>
			</section>

			<section class="content container-fluid" id="contentDiv">
				<?php $this->load->view($content); ?>
			</section>
		</div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<!-- Anything you want -->
			</div>
			<div align="center">
				<strong>Universitas Jenderal Achmad Yani Yogyakarta</strong>
			</div>
		</footer>
		<div class="control-sidebar-bg"></div>
	</div>

	<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
	<script src="<?=base_url()?>assets/js/dataTables.bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/fastclick.js"></script>
	<script src="<?=base_url()?>assets/js/adminlte.min.js"></script>
	<script>
		$(function() {
			$("#btn-logout").click(function() {
				var r = confirm("Yakin untuk log out?");
				if (r == true) {
					window.location.replace('<?=base_url()?>main/doLogout');
				} 
			});
		});
	</script>
</body>
</html>