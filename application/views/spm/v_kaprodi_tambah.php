<form role="form" action="<?=base_url()?>spm/do_kaprodi_tambah" method="post">
  <div class="box box-success">
    <div class="box-header">
      <a href="<?=base_url()?>spm/kaprodi" class="btn btn-default pull-left"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
      <input type="submit" class="btn bg-maroon pull-right" id="inp-submit" value="Simpan" />
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>NPK</label>
            <input type="text" class="form-control" name="npk" required="true" value="<?=$f_npk?>" autofocus />
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="name" required="true" value="<?=$f_name?>" />
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" name="username" required="true" value="<?=$f_username?>" />
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password" required="true" />
          </div>
          <div class="form-group">
            <label>Ulangi Password</label>
            <input type="password" class="form-control" name="repassword" required="true" />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Telp</label>
            <input type="text" class="form-control" name="telp" value="<?=$f_telp?>" />
          </div>
          <div class="form-group">
            <label>Grup</label>
            <select name="grup_id" class="form-control">
              <option value="0">- Pilih Grup -</option>
              <?php foreach ($ref_grup as $k => $v) { ?>
              <option <?php echo $v->user_grup_id==$f_user_grup_id ? 'selected' : ''; ?> value="<?=$v->user_grup_id?>"><?=$v->user_grup_name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Jabatan</label>
            <select name="jabatan_id" class="form-control">
              <option value="0">- Pilih Jabatan -</option>
              <?php foreach ($ref_jabatan as $k => $v) { ?>
              <option <?php echo $v->jabatan_id==$f_jabatan_id ? 'selected' : ''; ?> value="<?=$v->jabatan_id?>"><?=$v->jabatan_name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Role</label>
            <input type="text" class="form-control"" name="role" value="Kaprodi" disabled="true" />
          </div>
        </div>
      </div>
    </div>
  </div>
</form>