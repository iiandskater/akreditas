<style>
.fa-check {
	color: #00d472;
}
.fa-close {
	color: red;
}
label:hover {
	cursor: pointer;
}
</style>

<div class="box box-info">
	<div class="box-header">
		<a href="<?=base_url()?>borang/keb_dok/<?=$id_elemen?>" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Kembali</a>
		<button class="btn bg-maroon pull-right" data-toggle="modal" data-target="#modal-updoc">+ Upload Dokumen</button>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>#</th>
					<th>Dokumen</th>
					<th>Oleh</th>
					<th>Status</th>
					<?php if($this->session->userdata('role')==1) { echo "<th>Aksi</th>"; } ?>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach($data_bo_dokumen as $k => $v) { ?> <tr>
					<td><?=$n?></td>
					<td><a href="<?=base_url()?>docs/<?=$v->file?>"><?=$v->nama?></a></td>
					<td><?=$v->oleh?></td>
					<td align="center"><i class="fa fa-fw <?php echo $v->status==1 ? 'fa-check' : 'fa-close'; ?>"></i></td>
					<?php if($this->session->userdata('role')==1) { ?>
						<td align="center">
							<button class="btn btn-warning btn-xs btn-status" title="Set Status" data-toggle="modal" data-target="#modal-status" d-nama="<?=$v->nama?>" d-id="<?=$v->id_dokumen?>"><i class="fa fa-fw fa-check-square"></i></button>
							<button class="btn btn-info btn-xs" title="Revisi" data-toggle="modal" data-target="#modal-updoc"><i class="fa fa-fw fa-pencil"></i></button>
							<button class="btn btn-danger btn-xs btn-hapus" title="Hapus" del-url='<?=base_url("borang/do_delete_dok/$v->id_keb_dok/$v->id_dokumen/$main_n")?>'><i class="fa fa-fw fa-trash"></i></button>
						</td>
					<?php } ?>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>

<form action="<?=base_url()?>borang/do_upload_dok/<?=$id_keb_dok?>/<?=$main_n?>" method="post" enctype="multipart/form-data">
	<div class="modal fade" id="modal-updoc" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Upload Dokumen Baru</h4>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="the-file">Dokumen Pendukung</label>
								<input type="file" name="theFile" id="the-file" accept=".doc,.docx,.xls,.xlsx,.pdf">
								<p class="help-block">Format yang diterima: .doc, .docx, .xls, .xlsx, .pdf</p>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<input type="submit" class="btn bg-maroon" value="Upload" />
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form action="<?=base_url()?>borang/do_status_dok/<?=$id_keb_dok?>/<?=$main_n?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id_dok" id="stat_id_dok">
	<div class="modal fade" id="modal-status" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-body">
						<table class="table">
							<tr>
								<th>Dokumen</th>
								<th>Set Status</th>
							</tr>
							<tr>
								<td>
									<b id="nama-dok"></b>
								</td>
								<td>
									<div class="form-group" align="center">
										<label style="margin-right: 20px;">
											<input type="radio" name="status" value="0">
											<i class="fa fa-fw fa-close"></i>
										</label>
										<label>
											<input type="radio" name="status" value="1" checked>
											<i class="fa fa-fw fa-check"></i>
										</label>
									</div>
								</td>
							</tr>
						</table>
						<div style="text-align: right;">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							<input type="submit" class="btn bg-maroon" value="Set Status" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$(".btn-status").click(function() {
			$("#nama-dok").html($(this).attr("d-nama"));
			$("#stat_id_dok").val($(this).attr("d-id"));
		});
	});
</script>