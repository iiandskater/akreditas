<style>
td {
	vertical-align: middle!important; 
}
.fa-check {
	color: #00d472;
}
.fa-close {
	color: red;
}
</style>
<div class="box box-info">
	<div class="box-header">
		<a href="<?=base_url()?>borang/elemen/<?=$id_master?>" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Kembali</a>
		<?php if($this->session->userdata('role')==1) { ?>
			<a href="<?=base_url()?>borang/keb_dok_tambah/<?=$id_elemen?>" class="btn bg-maroon pull-right">+ Tambah Kebutuhan Dokumen</a>
		<?php } ?>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>#</th>
					<th>Kebutuhan Dokumen</th>
					<th>PIC</th>
					<th>Deskripsi</th>
					<th>Update Data</th>
					<th>Dokumen</th>
					<?php if($this->session->userdata('role')==1) { echo "<th>Aksi</th>"; } ?>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach ($data_bo_keb_dok as $k => $v) { ?><tr>
					<td><?=$n?></td>
					<td><?=$v->kebutuhan?></td>
					<td><?=$v->pic?></td>
					<td><?=$v->deskripsi?></td>
					<td>
						<?php foreach ($v->dokumen as $e => $a) { ?>
							<div><i class="fa fa-fw <?php echo $a->status==1 ? 'fa-check' : 'fa-close'; ?>"></i><?=$a->nama?></div>
						<?php } ?>
					</td>
					<td align="center">
						<a href="<?=base_url()?>borang/dokumen/<?=$v->id_keb_dok?>/<?=$n?>" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-fw fa-file-text"></i> Kelola</a>
					</td>
					<?php if($this->session->userdata('role')==1) { ?>
						<td align="center">
							<a href="<?=base_url()?>borang/keb_dok_edit/<?=$v->id_keb_dok?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
							<button class="btn btn-danger btn-xs btn-hapus" title="Hapus" del-url='<?=base_url("borang/do_delete_keb_dok/$v->id_elemen/$v->id_keb_dok")?>'><i class="fa fa-fw fa-trash"></i></button>
						</td>
					<?php } ?>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>