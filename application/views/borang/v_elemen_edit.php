<div class="box box-info">
	<form method="post" action="<?=base_url()?>borang/do_elemen_edit/<?=$id_master?>/<?=$id_elemen?>">
		<div class="box-header">
			<a href="<?=base_url()?>borang/elemen/<?=$id_master?>" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
			<input type="submit" class="btn bg-maroon pull-right" value="Simpan" />
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Poin</label>
						<div class="input-group">
							<span class="input-group-addon"><?=$standar?>.</span>
							<input type="text" class="form-control" name="butir" required="true" autofocus value="<?=$f_butir?>" />
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label>Elemen Penilaian</label>
						<input type="text" class="form-control" name="elemen" required="true" value="<?=$f_elemen?>" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Bobot</label>
						<input type="text" class="form-control" name="bobot" value="<?=$f_bobot?>" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Nilai</label>
						<input type="text" class="form-control" name="skor" value="<?=$f_skor?>" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Justifikasi</label>
						<select class="form-control" name="justifikasi">
							<option value="Sangat Kurang" <?php echo $f_justifikasi=="Sangat Kurang" ? "selected":""; ?> >
								SANGAT KURANG
							</option>
							<option value="Kurang" <?php echo $f_justifikasi=="Kurang" ? "selected":""; ?> >
								KURANG
							</option>
							<option value="Cukup" <?php echo $f_justifikasi=="Cukup" ? "selected":""; ?> >
								CUKUP
							</option>
							<option value="Baik" <?php echo $f_justifikasi=="Baik" ? "selected":""; ?> >
								BAIK
							</option>
							<option value="Sangat Baik" <?php echo $f_justifikasi=="Sangat Baik" ? "selected":""; ?> >
								SANGAT BAIK
							</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Standard Nilai</label>
						<textarea class="form-control" rows="4" name="penilaian"><?=$f_penilaian?></textarea>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
