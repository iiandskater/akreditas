<?php

$role = $this->session->userdata('role'); 
if($role==0) { //admin
	$nav = array(
		0 => array(
			"id"    => "",
			"title" => "User",
			"url"   => base_url()."admin/user",
			"icon"  => "fa-user"
		),
		1 => array(
			"id"    => "",
			"title" => "Jabatan",
			"url"   => base_url()."admin/jabatan",
			"icon"  => "fa-suitcase"
		),
		2 => array(
			"id"    => "",
			"title" => "Grup",
			"url"   => base_url()."admin/grup",
			"icon"  => "fa-group"
		)
	);
} elseif ($role==1) { //spm
	$nav = array(
		0 => array(
			"id"    => "nav_spm_kaprodi",
			"title" => "Manajemen Kaprodi",
			"url"   => base_url()."spm/kaprodi",
			"icon"  => ""
		),
		1 => array(
			"id"    => "nav_spm_tahun",
			"title" => "Tahun Borang",
			"url"   => base_url()."spm/tahun",
			"icon"  => ""
		),
		2 => array(
			"id"    => "nav_spm_borang",
			"title" => "Isian Borang",
			"url"   => base_url()."borang/master",
			"icon"  => ""
		)
	);
} elseif ($role==2) { //kaprodi
	$nav = array(
		0 => array(
			"id"    => "nav_kap_panitia",
			"title" => "Manajemen Panitia",
			"url"   => base_url()."kaprodi/panitia",
			"icon"  => ""
		),
		1 => array(
			"id"    => "nav_kap_borang",
			"title" => "Isian Borang",
			"url"   => base_url()."borang/master",
			"icon"  => ""
		)
	);
} elseif ($role==3) { //panitia
	$nav = array(
		1 => array(
			"id"    => "nav_pan_borang",
			"title" => "Isian Borang",
			"url"   => base_url()."borang/master",
			"icon"  => ""
		)
	);
} elseif ($role==4) { //other
	$nav = array(

	);
}
?>

<li class="nav-link" id="nav_home"><a href="<?=base_url()?>">Home</a></li>

<?php foreach ($nav as $k => $v) { ?>
	<li class="nav-link" id="<?=$v['id']?>"><a href="<?=$v['url']?>"><?=$v['title']?></a></li>
<?php } ?>

<li class="nav-link" id="nav_spm_dokumen"><a href="<?=base_url()?>dokumen">Dokumen Pendukung</a></li>
