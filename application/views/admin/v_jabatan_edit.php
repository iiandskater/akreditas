<form role="form" action="<?=base_url()?>admin/do_jabatan_edit/<?=$jabatan_id?>" method="post">
  <div class="box box-success">
    <div class="box-header">
      <a href="<?=base_url()?>admin/jabatan" class="btn btn-default pull-left"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
      <input type="submit" class="btn bg-maroon pull-right" id="inp-submit" value="Simpan" />
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6 center-col">
          <div class="form-group">
            <label>Nama Jabatan</label>
            <input type="text" class="form-control" name="jabatan_name" required="true" value="<?=$f_jabatan_name?>" autofocus />
          </div>
        </div>
      </div>
    </div>
  </div>
</form>