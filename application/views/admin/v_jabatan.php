<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title hidden">Data Table</h3>
		<a href="<?=base_url()?>admin/jabatan_tambah" class="btn bg-maroon pull-right">+ Tambah jabatan</a>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>No</th>
					<th>Jabatan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach($data_jabatan as $k => $v) { ?>
				<tr>
					<td><?=$n?></td>
					<td><?=$v->jabatan_name?></td>
					<td align="center">
						<a href="<?=base_url()?>admin/jabatan_edit/<?=$v->jabatan_id?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
						<button class="btn btn-danger btn-xs btn-hapus" jabatan="<?=$v->jabatan_id?>" title="Hapus"><i class="fa fa-fw fa-trash"></i></button>
					</td>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(function () {
		$(".btn-hapus").click(function() {
			var r = confirm("Hapus data ini?");
			var id = $(this).attr("jabatan");

			if (r == true) {
				window.location.replace('<?=base_url()?>admin/do_jabatan_hapus/'+id);
			} 
		});
	})
</script>
