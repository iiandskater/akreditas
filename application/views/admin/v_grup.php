<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title hidden">Data Table</h3>
		<a href="<?=base_url()?>admin/grup_tambah" class="btn bg-maroon pull-right">+ Tambah grup</a>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>No</th>
					<th>Grup</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach($data_grup as $k => $v) { ?>
				<tr>
					<td><?=$n?></td>
					<td><?=$v->user_grup_name?></td>
					<td align="center">
						<a href="<?=base_url()?>admin/grup_edit/<?=$v->user_grup_id?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
						<button class="btn btn-danger btn-xs btn-hapus" grup="<?=$v->user_grup_id?>" title="Hapus"><i class="fa fa-fw fa-trash"></i></button>
					</td>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(function () {
		$(".btn-hapus").click(function() {
			var r = confirm("Hapus data ini?");
			var id = $(this).attr("grup");

			if (r == true) {
				window.location.replace('<?=base_url()?>admin/do_grup_hapus/'+id);
			} 
		});
	})
</script>
