<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_dokumen'));
	}

	public function index() {
		$data['htitle'] = "Dokumen Pendukung";
		$data['content'] = "dokumen/v_dokumen";
		$data['data_dokumen'] = $this->m_dokumen->get_dokumen();

		$this->load->view('v_main_top', $data);
	}
}